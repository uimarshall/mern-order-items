# mern order items

This is an application that allows a user to register, login and create orders consisting of multiple products. The application would send an email and SMS notification to the user for each order they create with details of the items selected.