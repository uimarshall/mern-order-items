var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ItemSchema = new Schema({
	title: { type: String, required: true },
	// we just give a path to the img bc the img will not be stored in the db
	img: { type: String, required: true },
	price: { type: Number, required: true },
	company: { type: String, required: true },
	description: { type: String, required: true },

	inCart: {
		type: Boolean,
		default: false
	},
	count: {
		type: Number,
		default: 0
	},
	total: {
		type: Number,
		default: 0
	},
	date: {
		type: Date,
		default: Date.now
	}
});

// Compile the ItemSchema into a model
const Item = mongoose.model("Item", ItemSchema);
module.exports = Item;
