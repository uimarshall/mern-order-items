import React, { Component } from "react";
import Item from "./Item";
import Title from "./Title";
// Connect ItemList to redux store
import { connect } from "react-redux";
import { getItems, deleteItem } from "../actions/itemActions";
import PropTypes from "prop-types";

class ItemList extends Component {
	// Comp(ItemList) dispatches an actn to d 'itemReducer' by calling the 'getItems()' fn
	// This will return the state of the store
	componentDidMount() {
		// If you bring in a method from 'Redux', it is stored as 'props'
		this.props.getItems();
	}

	// HandleDelete
	// The 'id' comes frm the db, and is dispatched as payload frm the actionCreator(itemActions)
	// The deleteItem() func does this dispatch
	onDeleteClick = id => {
		this.props.deleteItem(id);
	};
	render() {
		// Get state of the store, 'item = state of the store',while 'items in the reducer' reps the array
		// The 'array' of the store is now stored in 'items'
		const { items } = this.props.item;
		return (
			<React.Fragment>
				<div className="py-5">
					<div className="container">
						<Title name="get our" title="produts" />
						<div className="row">
							{items.map(item => (
								<Item item={item} key={item.id} />
							))}
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

ItemList.propTypes = {
	getItems: PropTypes.func.isRequired,
	// deleteItem: PropTypes.func.isRequired,
	item: PropTypes.object.isRequired
};
// The 'state' params gives us access 2 d state of the store
// 'state.item' is d whole state of the store, this from the rootReducer(index.js) which stores
// the itemReducer in a key called 'item' and the itemReducer fn is returning the state of the store
// so 'state.item' gives the whole state of the store, we then store it in 'item' which will
// be mapped to the props of the ShoppingList comp
const mapStateToProps = state => ({
	// 'state.item' comes from the combine reducer, item = itemReducer
	// 'item' contains the 'items' array in the reducer and is mapped to the state of the store
	item: state.item
});

// The connect fn take 2 params: mapStateToProps & and any action we want the comp to dispatch
// 'mapStateToProps' map the state(data) of the store to d 'props' of our comp
// so we can now access data from the store thru our props
export default connect(
	mapStateToProps,
	{ getItems, deleteItem }
)(ItemList);
