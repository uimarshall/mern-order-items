import React, { Component } from "react";
import { connect } from "react-redux";
import { addItem } from "../actions/itemActions";

class AddItem extends Component {
	state = {
		title: "",
		img: "",
		price: "",
		company: "",
		description: ""
	};
	onChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onSubmit = e => {
		e.preventDefault();
		const newItem = {
			title: this.state.title,
			img: this.state.img,
			price: this.state.pricecompany,
			company: this.state.company,
			description: this.state.description
		};

		// Add Item via addItem action
		this.props.addItem(newItem);
	};
	render() {
		return (
			<React.Fragment>
				<div className="row">
					{/* <h2 className="align-items-center mb-5 mx-auto">Add Item!!!</h2> */}
					<div style={{ width: "30%" }} className="mt-5 mb-10 mx-auto">
						<form onSubmit={this.onSubmit} className="py-10 mb-10">
							<div className="form-group">
								<label htmlFor="title">Title</label>
								<input
									type="text"
									className="form-control"
									name="title"
									id="title"
									placeholder="Item title"
									onChange={this.onChange}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="img">Product Image</label>
								<input
									type="text"
									className="form-control"
									name="img"
									id="img"
									placeholder="Add image"
									onChange={this.onChange}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="price">Price</label>
								<input
									type="number"
									className="form-control"
									name="price"
									id="price"
									placeholder="price"
									onChange={this.onChange}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="company">Company</label>
								<input
									type="text"
									className="form-control"
									name="company"
									id="company"
									placeholder="Manufacturer"
									onChange={this.onChange}
								/>
							</div>
							<div className="form-group">
								<label htmlFor="title">Description</label>
								<input
									type="text"
									className="form-control"
									name="description"
									id="description"
									placeholder="Item description"
									onChange={this.onChange}
								/>
							</div>
							<div
								class="
                        form-group"
							>
								<button class="btn btn-primary btn-block" type="submit">
									Submit!
								</button>
							</div>
						</form>
					</div>
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = state => ({
	item: state.item
});

export default connect(
	mapStateToProps,
	{ addItem }
)(AddItem);
