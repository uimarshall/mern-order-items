/* MODUS OPERANDI OF REDUX
If a comp want to subscribe for data in the central store(Redux):
1. The Comp will dispatch an action to the store('store.dispatch(actn)')
The actn describes the changes the comp want to MAKE e.g 'ADD_TODO'
=============================================================================
2. The ACTN is passed to the REDUCER, the REDUCER then updates the state
of the store based on the actn passed,
The reducer takes 2 params(state=initState,actn), so it takes the initialState of the store
and updates it to a new state using the action params.
=============================================================================
3. The COMP can then subscribe to the STORE, and the store will pass the data
to the COMP in form of props.
=============================================================================
*/

// 'store.js' is the entry point to our Redux store
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers";
const initialState = {};
/* Any Middleware we want to use should be put in a variable, e.g
'thunk'(middleware) is put into the array.*/

const middleware = [thunk];
const store = createStore(
	rootReducer,
	initialState,
	compose(
		applyMiddleware(...middleware),
		window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
);
export default store;
