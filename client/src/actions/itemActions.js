// This is where we make request to the backend
import axios from "axios";
import { GET_ITEMS, ADD_ITEM, DELETE_ITEM, ITEMS_LOADING } from "./actionTypes";
// dispatch action - type = GET_ITEMS
export const getItems = () => dispatch => {
	dispatch(setItemsLoading());
	axios.get("/api/items").then(res =>
		dispatch({
			type: GET_ITEMS,
			payload: res.data
		})
	);
};

export const addItem = item => dispatch => {
	axios.post("/api/items", item).then(res =>
		dispatch({
			/*The state of the 'store' will be updated wt the data(res.data) we get frm the backend
            which will be dispatched to the 'reducer',our comp can then subscribe to this data and act upon it*/
			type: ADD_ITEM,
			payload: res.data
		})
	);
};

// Delete Item Action: which will be dispatched
// Pass in the 'id' to be deleted as payload,
// the 'id' will come frm a comp,since its a comp that dispatches an action
export const deleteItem = id => dispatch => {
	axios.delete(`/api/items/${id}`).then(res =>
		dispatch({
			type: DELETE_ITEM,
			payload: id
		})
	);
};

export const setItemsLoading = () => {
	return {
		type: ITEMS_LOADING
	};
};
