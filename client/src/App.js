import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Navbar from "./components/Navbar";
import ItemList from "./components/ItemList";
import AddItem from "./components/AddItem";
import Details from "./components/Details";
// import Cart from "./components/cart";
import PageNotFound from "./components/PageNotFound";
// import Modal from "./components/Modal";

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<React.Fragment>
					<Navbar />
					<Switch>
						<Route exact path="/" component={ItemList} />
						<Route exact path="/details" component={Details} />
						{/* <Route exact path="/cart" component={Cart} /> */}
						<Route exact path="/admin" component={AddItem} />
						<Route component={PageNotFound} />
					</Switch>
					{/* <Modal /> */}
				</React.Fragment>
			</Provider>
		);
	}
}

export default App;
