import {
	GET_ITEMS,
	ADD_ITEM,
	DELETE_ITEM,
	ITEMS_LOADING
} from "../actions/actionTypes";
// import { storeProducts, detailProduct } from "../data";

const initState = {
	items: [],
	// detailProduct: detailProduct,
	cart: [],
	modalOpen: false,
	// modalProduct: detailProduct,
	cartSubtotal: 0,
	cartTax: 0,
	cartTotal: 0,

	// When we make the api call, b4 the data loads, we set loading to false
	loading: false
};

const myreducer = (state = initState, action) => {
	switch (action.type) {
		case GET_ITEMS:
			return {
				...state,
				items: action.payload,
				loading: false
			};
		case DELETE_ITEM:
			return {
				...state,
				items: state.items.filter(item => item._id !== action.payload)
			};
		case ADD_ITEM:
			return {
				...state,
				items: [action.payload, ...state.items]
			};
		case ITEMS_LOADING:
			return {
				...state,
				loading: true
			};
		default:
			return state;
	}
};
export default myreducer;
