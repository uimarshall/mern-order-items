/*'index.js' is the rootReducer and its to bring 2gether all our Reducers.
Our 'itemReducer' shall be called 'item' and will be responsible for 
updating the state of the item in the store*/
import { combineReducers } from "redux";
import itemReducer from "./itemReducer";
export default combineReducers({
	// 'item' will be accessed in the 'state' of the store in the reducer,it will hold the 'items' array
	item: itemReducer
});
